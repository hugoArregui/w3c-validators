import re
import argparse
import requests
from urllib import request, parse

from lxml import html
from termcolor import cprint


class Validator:
    color = {
        'Info': 'green',
        'Error': 'red',
        'Warning': 'yellow'
    }

    def __init__(self, args, _tmp_filename):
        self._args = args
        self._tmp_filename = _tmp_filename

    def run(self):
        if self._args.url:
            url = self._args.url
            if self._args.local:
                request.urlretrieve(url, self._tmp_filename)
                r = self.post_file(self._tmp_filename)
            else:
                r = self.post_url(url)
        else:
            r = self.post_file(self._args.file)
        for error in self.process_response(r.text):
            self._print_error(error)

    @classmethod
    def from_args(cls):
        parser = argparse.ArgumentParser()
        parser.add_argument('--local', dest='local', action='store_const',
                            const=True)
        parser.add_argument('--url', dest='url', action='store')
        parser.add_argument('--file', dest='file', action='store')
        args = parser.parse_args()

        if not args.file and not args.url:
            parser.print_usage()
            exit(-1)
        return cls(args)

