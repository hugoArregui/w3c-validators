#!/bin/env python3

import re
import argparse
import requests
from urllib import request, parse
from itertools import chain

from lxml import html
from termcolor import cprint

from lib.validator import Validator

from requests import Request, Session

from io import BytesIO

class CSSValidator(Validator):
    base_url = "http://jigsaw.w3.org/css-validator"
    default_params = {
        'profile':     "css3",
        'usermedium':  "all",
        'warning':     1,
        'vextwarning': "",
        'lang':        "en"
    }

    def __init__(self, args):
        Validator.__init__(self, args, '/tmp/validator.css')

    def post_file(self, filename):
        url = self.base_url + '/validator'
        f = open(filename, 'rb')
        r = requests.post(url,
                          files=[('text', ('', f))],
                          data=self.default_params)
        return r

    def post_url(self, url):
        params = dict(self.default_params, uri=url)
        request_url = self.base_url + '/validator?' + parse.urlencode(params)
        return requests.get(request_url)

    def process_response(self, r):
        def parse(item, type):
            text = item.xpath('.//td[3]/text()')[0].strip()
            context = item.xpath('.//td[@class="codeContext"]/text()')
            where = item.xpath('.//td[@class="linenumber"]/text()')[0]
            return {
                'type': type,
                'text': text,
                'context': context[0] if context else '',
                'where': where
            }
        doc = html.document_fromstring(r)
        return chain((parse(item, 'Error') 
                      for item in doc.xpath('//div[@id="errors"]//tr')),
                     (parse(item, 'Warning') 
                      for item in doc.xpath('//div[@id="warnings"]//tr')))

    def _print_error(self, error):
        msg = "{where:5} {context:20} {text}".format(**error)
        cprint(msg, self.color[error['type']])

CSSValidator.from_args().run()
