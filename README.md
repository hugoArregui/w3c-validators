# W3C validator clients

Python client for w3c validation services

## Validators:

- [HTML](https://validator.w3.org/nu/)
- [CSS](http://jigsaw.w3.org/css-validator/)

## Installation

python3 is required, lxml and termcolor packages too.

## Usage

- To validate a file:

    `<validator> --file <filename>`

- To validate a public url:

    `<validator> --url <url>`

- To validate a local url:

    `<validator> --local --url <url>`

  In this case the url will be downloaded and uploaded as a file

Validator must be `w3c-css-validator.py` or `w3c-html-validator.py`.
