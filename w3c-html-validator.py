#!/bin/env python3

import re
import argparse
import requests
from urllib import request, parse

from lxml import html
from termcolor import cprint

from lib.validator import Validator


class HTMLValidator(Validator):
    base_url = "https://validator.w3.org/nu/"

    def __init__(self, args):
        Validator.__init__(self, args, '/tmp/validator.html')

    def post_file(self, filename):
        url = self.base_url + "#textarea"
        f = open(filename, 'rb')
        return requests.post(url, files=[('POST_REQUEST', f)])

    def post_url(self, url):
        request_url = self.base_url + '?' + parse.urlencode(dict(doc=url))
        return requests.get(request_url)

    def process_response(self, r):
        def parse(item):
            location = item.xpath('.//p[@class="location"]')
            where = '----'
            if location:
                text = location[0].text_content()
                m = re.findall("line ([0-9]+), column ([0-9]+)", text)
                if len(m) == 2:
                    f, t = m
                    where = "From %s:%s to %s:%s" % (f[0], f[1],
                                                     t[0], t[1])
                else:
                    where = m[0]
                    where = "%s:%s" % (where[0], where[1])

            return {
                'type': item.xpath('.//strong/text()')[0],
                'text': item.xpath('.//span')[0].text_content(),
                'where': where
            }
        doc = html.document_fromstring(r)
        return [parse(item)
                for item in doc.xpath('//div[@id="results"]/ol/li')]

    def _print_error(self, error):
        msg = "{type:7}  {where:25} {text}".format(**error)
        cprint(msg, self.color[error['type']])

HTMLValidator.from_args().run()
